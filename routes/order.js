const Order = require('../models/Order.js');
const Coffee = require('../models/Coffee.js');
const stripeID = require('../config').stripeID;
const shippoID = require('../config').shippoID;
const stripe = require("stripe")(stripeID);
var shippo = require('shippo')(shippoID); 
var transporter = require('./mail').transporter; 

var apiEndpoint = require('../config').apiEndpoint;

module.exports = {
	getAll: (req, res) => {
		Order.find({})
			.populate({
				path: 'items.bean',
				populate: { path: 'roaster' }
			})
			.exec((err, orders) => err ? res.status(400).json(err) : res.status(200).json(orders));
	},
	prepareOne: (req, res) => {

		var addresses = [], quantities = [], chargeID = '';
		const token = req.body.tokenID;

		var shippingRateResolver = new Promise((resolve, reject) => {

			for (var i = 0; i < req.body.items.length; i++) {

				Coffee.findOne({ _id: req.body.items[i].bean })
					.exec((err, coffee) => {
						if (err) console.log(err);
						if (!coffee) console.log('coffee by requested id not found');

						addresses.push(coffee.shipping);

						if (addresses.length == req.body.items.length) {
							resolve(addresses)
						}
					});

			}
		}).then((addresses) => {

			var shipments = [];
			var shipment_data = [];
			var getRates = new Promise((resolve, reject) => {


				var addressFrom = [], parcel = []; 

				if (req.body.shipping.country_code != 'US') res.status(400).json({message: 'Shipping only in US supported'}); 

				var addressTo = {
					"object_purpose": "PURCHASE",
					"name": req.body.shipping.name,
					"street1": req.body.shipping.line1,
					"city": req.body.shipping.city,
					"state": req.body.shipping.state,
					"zip": req.body.shipping.zip,
					"country": req.body.shipping.country_code,
					"email": req.body.email
				}

				for (var i = 0; i < addresses.length; i++) {

					addressFrom.push({
						"object_purpose": "PURCHASE",
						"name": addresses[i].company_name,
						"street1": addresses[i].address,
						"city": addresses[i].city,
						"state": addresses[i].state,
						"zip": addresses[i].zip,
						"country": addresses[i].country,
						"phone": addresses[i].phone,
						"email": addresses[i].email

					})

					parcel.push({
						"length": parseFloat(addresses[i].length) * req.body.items[i].quantity,
						"width": addresses[i].width,
						"height": addresses[i].height,
						"distance_unit": "in",
						"weight": parseFloat(addresses[i].weight) * req.body.items[i].quantity,
						"mass_unit": "oz"
					});

					shippo.shipment.create({
						"object_purpose": "PURCHASE",
						"address_from": addressFrom[i],
						"address_to": addressTo,
						"parcel": parcel[i],
						"async": false
					}, function (err, shipment) { 

						if (err) {
							reject(err);
							
						}

						shipments.push(shipment)

						if (shipments.length == addresses.length) {
							resolve(shipments)
						}

					
					})

				}

			}).then((shipments) => {

				var data = [];
				for (var i = 0; i < shipments.length; i++) {

					var rates = [];
					if (!shipments[i]) res.status(400).json({message: 'Shipment rate calculation error'}); 
					if (shipments[i].rates_list.length == 0) {res.status(400).json({message: 'Shipment rate calculation error'})}
					var all_rates = shipments[i].rates_list.map((item) => {


					var shipping_rate = parseFloat(item.amount) * 100; // shipping rate in cents 

					var stripe_fee = (shipping_rate * 2.9)/100; // add stripe fee (2.9 percent + 30 cents)
					stripe_fee = Math.ceil(stripe_fee) + 30; 
					
					shipping_rate = (shipping_rate + stripe_fee) / 100; 

						if (item.servicelevel_token == 'usps_first' || item.servicelevel_token == 'usps_priority') {
							var rate = {
								service: item.servicelevel_name,
								amount: shipping_rate,
								currency: item.currency,
								id: item.object_id
							}
							rates.push(rate)
						}

						return rate;

					})

					data.push({
						id: shipments[i].object_id,
						rates: rates
					})
				}
				var order_data = Object.assign({}, req.body, { token: token });

				for (var i = 0; i < order_data.items.length; i++) {
					order_data.items[i].rates = data[i].rates;
					order_data.items[i].shipment_id = data[i].id;
				}

				res.status(200).json(order_data);

			}).catch(err => {
				res.status(400).json({message: 'Shipment rate calculation error'})
			})
		})

	},
	createOne: (req, res) => {
		const token = req.body.token;

		// pay with stripe 
		var charge = stripe.charges.create({
			amount: req.body.charge, // Amount in cents
			currency: "usd",
			source: token,
			description: "Charge for " + req.body.shipping.name
		}, function (err, charge) {

			if (err) res.status(400).json(err); 

			var chargeID = charge.id;
			var transactions = [], order_data = Object.assign({}, req.body, { chargeId: chargeID });
			for (var i = 0; i < req.body.items.length; i++) {

				shippo.transaction.create({
					"rate": req.body.items[i].rates.id,
					"label_file_type": "PDF",
					"async": false
				}, function (err, transaction) {

					var j = transactions.length;
					transactions.push(transaction);

					order_data.items[j].transaction_id = transaction.object_id;
					order_data.items[j].tracking_url = transaction.tracking_url_provider;
					order_data.items[j].label_url = transaction.label_url;

					if (transactions.length == req.body.items.length) {
						Order.create(order_data, (err, order) => {
							err ? res.status(400).json(err) : res.status(200).json(order)
						});
					}
				})
			}

		})
	},

	getOne: (req, res) => {
		Order.findOne({ _id: req.params.id })
			.populate('items')
			.exec((err, order) => {
				if (err) return res.status(400).json(err);
				if (!order) return res.status(404).json();
				res.status(200).json(order);
			});
	},
	updateOne: (req, res) => {
		Order.findOneAndUpdate({ _id: req.params.id }, req.body, (err, order) => {
			if (err) return res.status(400).json(err);
			if (!order) return res.status(404).json();
			res.status(200).json(order);
		});
	},
	deleteOne: (req, res) => {
		Order.findOneAndRemove({ _id: req.params.id }, (err) => {
			if (err) return res.status(400).json(err);
			res.status(204).json();
		});
	},
	cancelOne: (req, res) => {

		shippo.refund.create({
			"transaction": req.body.t_id,
			"async": false
		}, function (err, refund) {
			if (err) res.status(400).send({ message: 'Shipping label refund request error.' });

			console.log('refund for label ' + req.body.t_id);  

			Order.findOneAndRemove({ _id: req.body.order_id }, (err) => {
				if (err) res.status(400).json({ message: 'Order not found' }); 

				let mailOptions = {
					from: 'theworldscoffee@gmail.com',
					to: [req.body.email, req.body.bean.shipping.email//, 'theworldscoffee@protonmail.com'
					], 
					subject: 'The World\'s Coffee Order Cancelled', 
					html: `<h3>Order Data:</h3>

						<p><strong>Product name: </strong>${req.body.bean.name}</p>
						<p><strong>Quantity: </strong>${req.body.quantity}</p>

						<h4>Order has been cancelled</h4>
						`
				};

				transporter.sendMail(mailOptions, (error, info) => {
					if (error) {
						return console.log(error);
						res.status(400).send({ message: 'Order was successfully cancelled but failed to send notifications' });
					}
					console.log('Message %s sent: %s', info.messageId, info.response);
					transporter.close();
					res.status(200).send({ message: 'Order was successfully cancelled' });

				});
			});
	
		})
	}
}