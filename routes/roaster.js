const Roaster = require('../models/Roaster.js');
const Coffee = require('../models/Coffee.js');

module.exports = {
	getAll: (req, res) => {
		Roaster.find({})
			.populate('coffees')
			.exec((err, roasters) => err ? res.status(400).json(err) : res.status(200).json(roasters));
	},
	getMatch: (req, res) => {
		const searchTerm = req.query.search;
		Roaster.find(
	        { $text : { $search : searchTerm } }, 
	        { score : { $meta: "textScore" } }
	    )
		    .sort({ score : { $meta : 'textScore' } })
		    .populate('coffees')
			.exec((err, roasters) => err ? res.status(400).json(err) : res.status(200).json(roasters));
	},
	createOne: (req, res) => {
		console.log(req.body);
		Roaster.create(req.body, (err, roaster) => err ? res.status(400).json(err) : res.status(200).json(roaster));
	},
	getOne: (req, res) => {
		Roaster.findOne({_id: req.params.id})
			.populate('coffees')
			.exec((err, roaster) => {
				if(err) return res.status(400).json(err);
				if(!roaster) return res.status(404).json();
				res.status(200).json(roaster);
			});	
	},
	updateOne: (req, res) => {
		Roaster.findOneAndUpdate({_id: req.params.id}, req.body, (err, roaster) => {
			if(err) return res.status(400).json(err);
			if(!roaster) return res.status(404).json();
			res.status(200).json(roaster);
		});
	},
	deleteOne: (req, res) => {
		Coffee.remove({roaster: req.params.id}, (err) => {
			if(err) return res.status(400).json(err);
			Roaster.findOneAndRemove({_id: req.params.id}, (err) => {
				if(err) return res.status(400).json(err);
				res.status(204).json();
			});
		})
	},
}