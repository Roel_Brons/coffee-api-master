var Coffee = require('../models/Coffee.js');
var nodemailer = require('nodemailer');
var clientEndpoint = require('../config').clientEndpoint;

let transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'theworldscoffee@gmail.com',
        pass: 'coffee123'
    }
});


module.exports = {
    transporter: transporter, 
    sendPaymentVerification(req, res) {

        var beans = [];
        for (var i = 0; i < req.body.items.length; i++) {

            Coffee.findOne({ _id: req.body.items[i].bean })
                .exec((err, coffee) => {
                    if (err) console.log(err);
                    if (!coffee) console.log('coffee by requested id not found');

                    beans.push(coffee);

                    if (beans.length == req.body.items.length) {

                        for (var j = 0; j < req.body.items.length; j++) {

                            let mailOptions = {
                                from: 'theworldscoffee@gmail.com',
                                to: [req.body.email//, 'theworldscoffee@protonmail.com'
								], 
                                subject: 'The World\'s Coffee Order', 
                                html: `<h3>Order Data</h3> 
                                    <p><strong>Product name: </strong>${beans[j].name}</p>
                                    <p><strong>Quantity: </strong>${req.body.items[j].quantity}</p>
                                    <p><strong>Shipping label url: </strong><a href="${req.body.items[j].label_url}">${req.body.items[j].label_url}</a></p>
                                    <p><strong>Package tracking url: </strong><a href="${req.body.items[j].tracking_url}">${req.body.items[j].tracking_url}</a></p>
                                    `
                            };


                            transporter.sendMail(mailOptions, (error, info) => {
                                if (error) {
                                    return console.log(error);
                                }
                                console.log('Message %s sent to client: %s', info.messageId, info.response); 
                                transporter.close();
                            });



                            let distributorMailOptions = {
                                from: 'theworldscoffee@gmail.com',
                                to: beans[j].shipping.email,
                                subject: 'The World\'s Coffee Order', 
                                html: `<h3>Order Data</h3>
                                    <p><strong>Product name: </strong>${beans[j].name}</p>
                                    <p><strong>Quantity: </strong>${req.body.items[j].quantity}</p>
                                    <p><strong>Shipping label url: </strong><a href="${req.body.items[j].label_url}">${req.body.items[j].label_url}</a></p>
                                    <p><strong>Package tracking url: </strong><a href="${req.body.items[j].tracking_url}">${req.body.items[j].tracking_url}</a></p>
                                    <p><strong>Cancel order: </strong><a href="${clientEndpoint}#/cancel/${req.body.items[j].bean}?email=${req.body.email}&q=${req.body.items[j].quantity}&rate=${req.body.items[j].rates.amount}&t_id=${req.body.items[j].transaction_id}&o_id=${req.body._id}">${clientEndpoint}/cancel/${req.body.items[j].bean}</a></p>
                                    `
                            };

                            transporter.sendMail(distributorMailOptions, (error, info) => {
                                if (error) {
                                    return console.log(error);
                                }
                                console.log('Message %s sent to distributor: %s', info.messageId, info.response);
                                transporter.close();
                            });


                        }
                    }
                });
        }

        res.status(200);

    }
}