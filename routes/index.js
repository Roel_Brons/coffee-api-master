const router = require('express').Router();
const roaster = require('./roaster');
const coffee = require('./coffee');
const order = require('./order');
const featured = require('./featured'); 
const mail = require('./mail'); 
const path = require('path'); 

router.get('/',(req, res) => {
	let a = __dirname.split('/');
	a = a.slice(0,a.length-1).join('/')
	res.sendFile(path.join(__dirname + '/../index.html'))
})

// Roaster routes
router.get('/roaster', (req, res) => {
	if(req.query.search) return roaster.getMatch(req, res);
	else roaster.getAll(req, res);
});
router.post('/roaster', roaster.createOne);
router.get('/roaster/:id', roaster.getOne);
router.put('/roaster/:id', roaster.updateOne);
router.delete('/roaster/:id', roaster.deleteOne);

// Coffee routes
router.get('/coffee', (req, res) => {
	if(req.query.search) return coffee.getMatch(req, res);
	else if(req.query.beans) return coffee.getMultiple(req, res);
	else return coffee.getAll(req, res);
});
router.post('/coffee', coffee.createOne);
router.get('/coffee/:id', coffee.getOne);
router.put('/coffee/:id', coffee.updateOne);
router.delete('/coffee/:id', coffee.deleteOne);

// Order routes
router.get('/order', order.getAll);
router.post('/order', order.createOne); 
router.get('/order/:id', order.getOne);
router.put('/order/:id', order.updateOne);
router.delete('/order/:id', order.deleteOne);  
router.post('/cancel', order.cancelOne); 

// Calculate shipping costs 
router.post('/shipping', order.prepareOne); 

// Featured routes
router.get('/featured', featured.getAll);
router.post('/featured', featured.createOne);
router.get('/featured/:id', featured.getOne);
router.put('/featured/:id', featured.updateOne);
router.delete('/featured/:id', featured.deleteOne); 

// Email verification
router.post('/mail', mail.sendPaymentVerification); 




module.exports = router;
