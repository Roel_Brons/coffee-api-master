const Featured = require('../models/Featured.js');

module.exports = {
	getAll: (req, res) => {
		Featured.find({})
			.exec((err, featured) => err ? res.status(400).json(err) : res.status(200).json(featured));
	},
	createOne: (req, res) => {
		console.log(req.body);
		Featured.create(req.body, (err, featured) => err ? res.status(400).json(err) : res.status(200).json(featured));
	},
	getOne: (req, res) => {
		Featured.findOne({_id: req.params.id})
			.exec((err, featured) => {
				if(err) return res.status(400).json(err);
				if(!featured) return res.status(404).json();
				res.status(200).json(featured);
			});	
	},
	updateOne: (req, res) => {
		Featured.findOneAndUpdate({_id: req.params.id}, req.body, (err, featured) => {
			if(err) return res.status(400).json(err);
			if(!featured) return res.status(404).json();
			res.status(200).json(featured);
		});
	},
	deleteOne: (req, res) => {
		Featured.findOneAndRemove({_id: req.params.id}, (err) => {
			if(err) return res.status(400).json(err);
			res.status(204).json();
		});
	},
}