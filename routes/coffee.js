const Coffee = require('../models/Coffee.js');
const Roaster = require('../models/Roaster.js');

module.exports = {
	getAll: (req, res) => {
		Coffee.find({})
			.populate('roaster')
			.exec((err, coffees) => err ? res.status(400).json(err) : res.status(200).json(coffees));
	},
	getMultiple: (req, res) => {
		const beanArray = req.query.beans.split(',');
		Coffee.find({_id: { $in: beanArray}})
			.populate('roaster')
			.exec((err, coffees) => err ? res.status(400).json(err) : res.status(200).json(coffees));
	},
	getMatch: (req, res) => {
		const searchTerm = req.query.search;
		Coffee.find(
	        { $text : { $search : searchTerm } }, 
	        { score : { $meta: "textScore" } }
	    )
		    .sort({ score : { $meta : 'textScore' } })
		    .populate('roaster')
			.exec((err, coffees) => err ? res.status(400).json(err) : res.status(200).json(coffees));
	},
	createOne: (req, res) => {
		console.log(req.body);
		Coffee.create(req.body, (err, coffee) => {
			Roaster.findByIdAndUpdate(req.body.roaster, {$push: {coffees: coffee._id}}, (err, res) => {
				if(err) console.log(err);
			} )
			return err ? res.status(400).json(err) : res.status(200).json(coffee)
		});
	},
	getOne: (req, res) => {
		console.log('Looking for coffee', req.params.id);
		Coffee.findOne({_id: req.params.id})
			.populate('roaster')
			.exec((err, coffee) => {
				if(err) return res.status(400).json(err);
				if(!coffee) return res.status(404).json();
				res.status(200).json(coffee);
			});
	},
	updateOne: (req, res) => {
		Coffee.findOneAndUpdate({_id: req.params.id}, req.body, (err, coffee) => {
			if(err) return res.status(400).json(err);
			if(!coffee) return res.status(404).json();
			res.status(200).json(coffee);
		});
	},
	deleteOne: (req, res) => {
		Coffee.findOneAndRemove({_id: req.params.id}, (err) => {
			if(err) return res.status(400).json(err);
			res.status(204).json();
		});
	},
}