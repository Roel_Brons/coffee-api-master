const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const {port, mongoURI, mongoDB} = require('./config');
const router = require('./routes');

const app = express();

// There was a problem with using the '*' wildcard, somthing about the credentials flag being set to true. 
// Setting it to 'req.headers.origin' gets around this, but shouldn't be used on the live site.
app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", req.headers.origin);
	res.header("Access-Control-Allow-Credentials", true);
	res.header("Access-Control-Allow-Methods", 'GET, POST, PUT, DELETE');
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

mongoose.connect(`${mongoURI}/${mongoDB}`);

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(router);

app.listen(port, () => console.log('Now listening on port '+port));