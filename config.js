module.exports = {
	port: process.env.PORT || 5000,
	mongoURI: process.env.MONGO_URI || process.env.MONGODB_URI || 'mongodb://localhost:27017',
	mongoDB: 'coffee-api',
	stripeID: "sk_test_EkAkgGPZoUHUzQUzLK4Bhl02", 
	shippoID: "shippo_test_240d0ea68abb77c3fe04becf8c0691f32eb128ef", 
	apiEndpoint: "http://localhost:" + process.env.PORT || 5000, 
	clientEndpoint: "http://localhost:3000"

}