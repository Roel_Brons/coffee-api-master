var mongoose = require('mongoose');

var coffeeSchema = mongoose.Schema({
	name: { type: String, required: true },
	description: {type: String },
	roaster : { type: mongoose.Schema.Types.ObjectId, ref: 'Roaster' },
	roast: { type: String, default: 'other' },
	photoUrl: {type: String, default: 'https://cdn0.iconfinder.com/data/icons/eco-overcolor/512/coffee_bean-512.png'},
	price: { type: Number, required: true }, 
	original_price: { type: Number, required: true }, 
	shipping: {
		company_name: { type: String, required: true }, 
		address: {type: String, required: true}, 
		state: {type: String, required: true}, 
		zip: {type: Number, required: true}, 
		country: {type: String, required: true}, 
		city: {type: String, required: true}, 
		phone: {type: String }, 
		email: {type: String, required: true}, 
		weight: {type: String}, 
		length: {type: String}, 
		height: {type: String}, 
		width: {type: String}
	}, 
	createdAt: { type: Date, default: Date.now() }
});
coffeeSchema.index({ name: 'text', description: 'text' });
var Coffee = mongoose.model('Coffee', coffeeSchema);
module.exports = Coffee;