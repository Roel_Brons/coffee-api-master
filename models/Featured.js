var mongoose = require('mongoose');

var featuredSchema = mongoose.Schema({
	name: { type: String, default: 'Featured Content' },
	url: {type: String, default: ''},
	photoUrl: {type: String, default: 'https://placehold.it/700x300'},
	createdAt: { type: Date, default: Date.now() }
});

var Featured = mongoose.model('Featured', featuredSchema);
module.exports = Featured;