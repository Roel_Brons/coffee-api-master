var mongoose = require('mongoose');

var roasterSchema = mongoose.Schema({
	name: { type: String, required: true },
	location: {
		lat: { type: Number, default: 0 },
		lng: { type: Number, default: 0 },
		city: {type: String, default: 'none' },
		state: {type: String, default: 'none' }
	},
	url: { type: String },
	photoUrl: { type: String, default: 'https://freeiconshop.com/files/edd/coffee-flat.png' },
	shortDescription: { type: String },
	story: { type: String },
	createdAt: { type: Date, default: Date.now() },
	coffees: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Coffee'}]
});
roasterSchema.index({ name: 'text', shortDescription: 'text', "location.state": 'text', "location.city": 'text' });
var Roaster = mongoose.model('Roaster', roasterSchema);
module.exports = Roaster;