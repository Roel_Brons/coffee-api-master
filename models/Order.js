var mongoose = require('mongoose');

var orderSchema = mongoose.Schema({
	email: {type: String, require: true},
	shipping: {
		city : {type: String},
		country : {type: String},
		country_code : {type: String},
		line1 : {type: String},
		state : {type: String},
		zip : {type: String},
		name : {type: String}
		
	},
	items: [{
		bean: {type: mongoose.Schema.Types.ObjectId, ref: 'Coffee'},
		rates: {type: Object}, 
		shipment_id: {type: String}, 
		transaction_id: {type: String}, 
		tracking_url: {type: String}, 
		label_url: {type: String}, 
		quantity: {type: Number, default: 1}
	}],
	orderStatus: {type: Number, default: 0},
	paymentStatus: {type: Boolean, default: true},
	createdAt: { type: Date, default: Date.now() },
	chargeId: { type: String, default: '' } 
});
// Status 0: Order has not been processed.
// Status 1: Order is currently being worked on.
// Status 2: Order has been shipped.

var Order = mongoose.model('Order', orderSchema);
module.exports = Order;